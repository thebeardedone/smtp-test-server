package api

import (
	"github.com/emersion/go-sasl"
	"github.com/emersion/go-smtp"
	"github.com/stretchr/testify/assert"
	"net/http"
	"strconv"
	"strings"
	"testing"
)

const(
	subject = "This is a subject!"
	body = "This is my email body!"
	recipient = "receipient@domain.com"
)

// Test sending an email
func TestIntegrationSendEmail(tester *testing.T) {
	setUpTest(tester)
	defer tearDownTest(tester)

	testContext := getTestContext()
	authentication := sasl.NewPlainClient("", testContext.Config.SMTPCredentials.Username, testContext.Config.SMTPCredentials.Password)
	msg := strings.NewReader("From: \"" + testContext.Config.SMTPCredentials.Username +
			"\" <" + testContext.Config.SMTPCredentials.Username + "@" + testContext.Config.Domain + ">\n" +
			"To: " + recipient + "\n" +
			"Subject: " + subject + "\n" +
			"MIME-version: 1.0;\nContent-Type: text/html; charset=\"UTF-8\";\n\n" +
			body)
	smtpError := smtp.SendMail(
		testContext.getSMTPServerUrl(), authentication, testContext.Config.SMTPCredentials.Username, []string{recipient}, msg)

	assert.Nil(tester, smtpError, "no error should be returned by the smtp server")
}

// When you send an email it should be added to a queue which you should be able to retrieve via the email end point
func TestIntegrationPopEmail(tester *testing.T) {
	setUpTest(tester)
	defer tearDownTest(tester)

	testContext := getTestContext()
	authentication := sasl.NewPlainClient("", testContext.Config.SMTPCredentials.Username, testContext.Config.SMTPCredentials.Password)
	msg := strings.NewReader("Subject: " + subject + "\n\n" + body)
	smtpError := smtp.SendMail(
		testContext.getSMTPServerUrl(), authentication, testContext.Config.SMTPCredentials.Username, []string{recipient}, msg)

	assert.Nil(tester, smtpError, "no error should be returned by the smtp server")

	response, getErr := http.Get(testContext.getAPIServerUrl() + "/email")
	assert.Nil(tester, getErr, "server request should not return an error")
	assert.Equal(tester, response.StatusCode, http.StatusOK, "http status should be OK - 200")
	email := parseEmailFromResponse(tester, response)

	assert.Equal(tester, email.Subject, subject, "the subject returned should equal the subject sent")
	assert.Equal(tester, email.TextBody, body, "the body sent should equal the body sent")
}

// Attempting to retrieve an email via an end point if no email is in the queue returns an error
func TestIntegrationPopEmailEmpty(tester *testing.T) {
	setUpTest(tester)
	defer tearDownTest(tester)

	testContext := getTestContext()
	response, getErr := http.Get(testContext.getAPIServerUrl() + "/email")
	assert.Nil(tester, getErr, "server request should not return an error")
	assert.Equal(tester, response.StatusCode, http.StatusNotFound, "http status should be Not Found - 404")
	message := parseErrorMessageFromResponse(tester, response)

	assert.Equal(tester, message, "failed to retrieve email", "the subject returned should equal the subject sent")
}

// Attempting to retrieve an email via an end point after all emails have already been retrieved will return an error
func TestIntegrationPopEmailEmptiedQueue(tester *testing.T) {
	setUpTest(tester)
	defer tearDownTest(tester)

	testContext := getTestContext()
	authentication := sasl.NewPlainClient("", testContext.Config.SMTPCredentials.Username, testContext.Config.SMTPCredentials.Password)
	msg := strings.NewReader("Subject: " + subject + "\n\n" + body)
	smtpError := smtp.SendMail(
		testContext.getSMTPServerUrl(), authentication, testContext.Config.SMTPCredentials.Username, []string{recipient}, msg)

	assert.Nil(tester, smtpError, "no error should be returned by the smtp server")

	response, getErr := http.Get(testContext.getAPIServerUrl() + "/email")
	assert.Nil(tester, getErr, "server request should not return an error")
	assert.Equal(tester, response.StatusCode, http.StatusOK, "http status should be OK - 200")
	email := parseEmailFromResponse(tester, response)

	assert.Equal(tester, email.Subject, subject, "the subject returned should equal the subject sent")
	assert.Equal(tester, email.TextBody, body, "the body sent should equal the body sent")

	response, getErr = http.Get(testContext.getAPIServerUrl() + "/email")
	assert.Nil(tester, getErr, "server request should not return an error")
	assert.Equal(tester, response.StatusCode, http.StatusNotFound, "http status should be Not Found - 404")
	message := parseErrorMessageFromResponse(tester, response)

	assert.Equal(tester, message, "failed to retrieve email", "the subject returned should equal the subject sent")
}

// The endpoint will return emails in the order that they were received by the server
func TestIntegrationPopEmailValidateOrder(tester *testing.T) {
	setUpTest(tester)
	defer tearDownTest(tester)

	testContext := getTestContext()
	authentication := sasl.NewPlainClient("", testContext.Config.SMTPCredentials.Username, testContext.Config.SMTPCredentials.Password)

	for i := 1; i <= 2; i++ {
		msg := strings.NewReader("Subject: " + subject + strconv.Itoa(i) + "\n\n" + body + strconv.Itoa(i))
		smtpError := smtp.SendMail(
			testContext.getSMTPServerUrl(), authentication, testContext.Config.SMTPCredentials.Username, []string{recipient}, msg)

		assert.Nil(tester, smtpError, "no error should be returned by the smtp server")
	}

	for i := 1; i <= 2; i++ {
		response, getErr := http.Get(testContext.getAPIServerUrl() + "/email")
		assert.Nil(tester, getErr, "server request should not return an error")
		assert.Equal(tester, response.StatusCode, http.StatusOK, "http status should be OK - 200")
		email := parseEmailFromResponse(tester, response)

		assert.Equal(tester, email.Subject, subject + strconv.Itoa(i), "the subject returned should equal the subject sent")
		assert.Equal(tester, email.TextBody, body + strconv.Itoa(i), "the body sent should equal the body sent")
	}

	response, getErr := http.Get(testContext.getAPIServerUrl() + "/email")
	assert.Nil(tester, getErr, "server request should not return an error")
	assert.Equal(tester, response.StatusCode, http.StatusNotFound, "http status should be Not Found - 404")
	message := parseErrorMessageFromResponse(tester, response)

	assert.Equal(tester, message, "failed to retrieve email", "the subject returned should equal the subject sent")
}