package api

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/thebeardedone/smtp-test-server/utils"
	"net/http"
)

var failedToRetrieveEmailErrorMessage = "failed to retrieve email"

// Retrieves and removes the first email received from the queue stored on the SMTP server.
// A 404 error is returned if no emails are available.
func PopEmail(smtpServer *utils.SMTPBackEnd, route string) http.HandlerFunc {
	return func(writer http.ResponseWriter, reader *http.Request) {

		email, queueError := smtpServer.GetEmail()

		if queueError != nil {
			logrus.WithFields(logrus.Fields{
				"error": queueError.Error(),
				"route": route,
			}).Error(failedToRetrieveEmailErrorMessage)

			utils.OnError(writer, http.StatusNotFound, failedToRetrieveEmailErrorMessage)
			return
		}

		logrus.Info("successfully retrieved email")
		utils.OnSuccess(writer, email)
	}
}