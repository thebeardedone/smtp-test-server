package api

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/thebeardedone/smtp-test-server/utils"
	"net/http"
)

// Deletes all emails queued on the smtp server
func DeleteEmails(smtpServer *utils.SMTPBackEnd, route string) http.HandlerFunc {
	return func(writer http.ResponseWriter, reader *http.Request) {
		smtpServer.ClearEmails()
		logrus.WithFields(logrus.Fields{
			"route": route,
		}).Info("successfully cleared all emails")
		utils.OnSuccess(writer, "successfully cleared all emails")
	}
}
