package api

import (
	"github.com/emersion/go-sasl"
	"github.com/emersion/go-smtp"
	"github.com/stretchr/testify/assert"
	"net/http"
	"strings"
	"testing"
)

// Calling the delete emails end point will remove any queued emails
func TestIntegrationDeleteEmails(tester *testing.T) {
	setUpTest(tester)
	defer tearDownTest(tester)

	testContext := getTestContext()
	authentication := sasl.NewPlainClient("", testContext.Config.SMTPCredentials.Username, testContext.Config.SMTPCredentials.Password)
	msg := strings.NewReader("Subject: " + subject + "\n\n" + body)
	smtpError := smtp.SendMail(
		testContext.getSMTPServerUrl(), authentication, testContext.Config.SMTPCredentials.Username, []string{recipient}, msg)

	assert.Nil(tester, smtpError, "no error should be returned by the smtp server")

	response, deleteErr := deleteEmails()
	assert.Nil(tester, deleteErr, "server request should not return an error")
	assert.Equal(tester, response.StatusCode, http.StatusOK, "http status should be OK - 200")

	response, getErr := http.Get(testContext.getAPIServerUrl() + "/email")
	assert.Nil(tester, getErr, "server request should not return an error")
	assert.Equal(tester, response.StatusCode, http.StatusNotFound, "http status should be Not Found - 404")
	message := parseErrorMessageFromResponse(tester, response)

	assert.Equal(tester, message, "failed to retrieve email", "the subject returned should equal the subject sent")
}

// Calling the delete emails end point will not return an error if the queue is empty
func TestIntegrationDeleteEmailsEmpty(tester *testing.T) {
	setUpTest(tester)
	defer tearDownTest(tester)

	response, deleteErr := deleteEmails()
	assert.Nil(tester, deleteErr, "server request should not return an error")
	assert.Equal(tester, response.StatusCode, http.StatusOK, "http status should be OK - 200")
}