package api

import (
	"encoding/json"
	"github.com/DusanKasan/parsemail"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.com/thebeardedone/smtp-test-server/config"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"testing"
)

// The TestConfiguration model specifies:
//	* APIPort: the port on which the api server is listening
//	* BaseUrl: the server (hostname or ip address) location
//	* Domain: the domain the SMTP server is hosting for
//	* SMTPCredentials: the SMTP credentials used to authenticate
//	* SMTPPort:	the port on which the SMTP server is listening
type TestConfiguration struct {
	APIPort			int						`json:"apiPort"`
	BaseUrl			string					`json:"baseUrl"`
	Domain			string					`json:"domain"`
	SMTPCredentials	config.SMTPCredentials	`json:"smtpCredentials"`
	SMTPPort		int						`json:"smtpPort"`
}

// TestContext contains references to objects that may be used by test cases
type TestContext struct {
	Config	TestConfiguration
}

// TestContext constructor which reads a file and un-marshals it into an object
func newTestContext(fileLocation string) TestContext {
	var testConfig TestConfiguration

	configFile, err := os.Open(fileLocation)
	byteValue, _ := ioutil.ReadAll(configFile)

	if err != nil {
		logrus.Error(err.Error())
		logrus.Panic("failed to find configuration file")
	}

	unmarshalError := json.Unmarshal(byteValue, &testConfig)

	if unmarshalError != nil {
		logrus.Error(unmarshalError.Error())
		logrus.Panic("failed to unmarshal server configuration")
	}

	return TestContext{
		Config: testConfig,
	}
}

// Returns the full API Server url
func (context *TestContext) getAPIServerUrl() string {
	return "http://" + context.Config.BaseUrl + ":" + strconv.Itoa(context.Config.APIPort)
}

// Returns the SMTP connection url
func (context *TestContext) getSMTPServerUrl() string {
	return context.Config.BaseUrl + ":" + strconv.Itoa(context.Config.SMTPPort)
}

var testContext TestContext

// When running the test suite, setUpSuite is called prior to running the tests.
func TestMain(m *testing.M) {
	setUpSuite()
	retCode := m.Run()
	os.Exit(retCode)
}

// The current test case name is printed prior to running the test
func setUpTest(tester *testing.T) {
	logrus.Debug("Preparing for " + tester.Name())
}

// All SMTP emails are removed after each test
func tearDownTest(tester *testing.T) {
	logrus.Debug("Cleaning up for " + tester.Name())
	_, _ = deleteEmails()
}

// Sets up the test suite by initializing the logger and reading the test configuration file
func setUpSuite() {
	logrus.SetLevel(logrus.DebugLevel)
	logrus.SetFormatter(&logrus.JSONFormatter{})

	testContext = newTestContext("../test-config.json")
}

// Returns the current test context
func getTestContext() *TestContext {
	return &testContext
}

// Helper function used to read the body of a response and returns it as a byte array
// The test will fail if the parser returns an error.
func readBody(tester *testing.T, body io.ReadCloser) []byte {
	defer body.Close()
	responseBody, bodyReadError := ioutil.ReadAll(body)
	assert.Nil(tester, bodyReadError, "body parser should not return an error")
	return responseBody
}

// Helper function used to unmarshal the body of a response into an email which is returned
// The test will fail if the parser returns an error or if unmarshalling fails.
func parseEmailFromResponse(tester *testing.T, response *http.Response) parsemail.Email {
	var email parsemail.Email
	unmarshalError := json.Unmarshal(readBody(tester, response.Body), &email)
	assert.Nil(tester, unmarshalError, "failed to unmarshal email")
	return email
}

// Helper function used to unmarshal the body of a response into an error message which is returned
// The test will fail if the parser returns an error or if unmarshalling fails.
func parseErrorMessageFromResponse(tester *testing.T, response *http.Response) string {
	var errorResponse map[string]string
	unmarshalError := json.Unmarshal(readBody(tester, response.Body), &errorResponse)
	assert.Nil(tester, unmarshalError, "failed to unmarshal error response")
	return errorResponse["error"]
}

// Helper function which sends a delete http request to the api server to delete queued emails, the response is returned
// to the caller
// Any error returned by the client or when building the request is returned to the caller.
func deleteEmails() (*http.Response, error) {
	request, requestError := http.NewRequest(http.MethodDelete, testContext.getAPIServerUrl() + "/emails", nil)
	if requestError != nil {
		return nil, requestError
	}
	client := &http.Client{}
	return client.Do(request)
}