module gitlab.com/thebeardedone/smtp-test-server

go 1.13

require (
	github.com/DusanKasan/parsemail v1.2.0
	github.com/emersion/go-sasl v0.0.0-20200509203442-7bfe0ed36a21
	github.com/emersion/go-smtp v0.14.0
	github.com/gorilla/mux v1.8.0
	github.com/sirupsen/logrus v1.7.0
	github.com/stretchr/testify v1.2.2
)
