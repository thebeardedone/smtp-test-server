package config

type SMTPCredentials struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type SMTPConnection struct {
	ReadTimeout		int	`json:"readTimeout"`
	WriteTimeout	int	`json:"writeTimeout"`
	MaxMessageBytes	int	`json:"maxMessageBytes"`
	MaxRecipients	int	`json:"maxRecipients"`
}

type SMTP struct {
	Credentials   	SMTPCredentials `json:"credentials"`
	Domain			string          `json:"domain"`
	GetEmailTimeout	int				`json:"getEmailTimeout"`
	Port          	int             `json:"port"`
	SMTPConnection	SMTPConnection	`json:"smtpConnection"`
}