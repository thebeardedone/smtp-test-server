package config

// Application model containing:
//	* AppName: application name
//	* Port:	port on which the application will listen on
type Application struct {
	AppName         string 		`json:"appName"`
	Port         	int    		`json:"port"`
}