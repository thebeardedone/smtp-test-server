package config

import (
	"net/http"
)

// ServerConfiguration is a model that defines:
//	* Application: the application configuration model
//	* SMTP:	the SMTP configuration model
type ServerConfiguration struct {
	Application		Application `json:"application"`
	SMTP			SMTP        `json:"smtp"`
}

// ServerContext holding references to:
//	* Config: a reference to the application configuration
//	* HttpServer: a reference to the http server instance
type Context struct {
	Config     Application
	HttpServer http.Server
}
