package main

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"gitlab.com/thebeardedone/smtp-test-server/api"
	"gitlab.com/thebeardedone/smtp-test-server/config"
	"gitlab.com/thebeardedone/smtp-test-server/utils"
	"io/ioutil"
	"net/http"
	"os"
)

// Reads the configuration file in the specified file location and attempts unmarshal the content into a
// ServerConfiguration object
func readConfigurationFile(fileLocation string) config.ServerConfiguration {
	serverConfig := config.ServerConfiguration{}

	configFile, err := os.Open(fileLocation)
	byteValue, _ := ioutil.ReadAll(configFile)

	if err != nil {
		logrus.Error(err.Error())
		logrus.Panic("failed to find configuration file")
	}

	unmarshalError := json.Unmarshal(byteValue, &serverConfig)

	if unmarshalError != nil {
		logrus.Error(unmarshalError.Error())
		logrus.Panic("failed to unmarshal server configuration")
	}
	return serverConfig
}

// Main function invoked.
// On execution the logger is initialized and the configuration file is read. A SMTP and http server are started in
// separate go-routines. The program then waits for a kill signal; once the signal is sent the SMTP and http server are
// stopped.
func main() {
	logrus.SetLevel(logrus.DebugLevel)
	logrus.SetFormatter(&logrus.JSONFormatter{})

	serverConfig := readConfigurationFile("./smtp-server.json")

	smtpBackEnd := utils.NewSMTPBackEnd(serverConfig.SMTP)

	smtpBackEnd.Start()

	serverContext := config.Context{
		Config: serverConfig.Application,
	}

	router := mux.NewRouter()
	router.HandleFunc("/email", api.PopEmail(smtpBackEnd, "pop email")).Methods(http.MethodGet)
	router.HandleFunc("/emails", api.DeleteEmails(smtpBackEnd, "delete emails")).Methods(http.MethodDelete)

	utils.Start(&serverContext, router)
	<-utils.WaitForStopSignal()

	utils.Stop(&serverContext)
	smtpBackEnd.Stop()
}
