package utils

import (
	"context"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"gitlab.com/thebeardedone/smtp-test-server/config"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"syscall"
	"time"
)

// Starts the http server supplied in the serverContext in a separate go-routine
func Start(serverContext *config.Context, router *mux.Router) {

	serverContext.HttpServer = http.Server{
		Addr:         ":" + strconv.Itoa(serverContext.Config.Port),
		Handler:      router,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 5 * time.Second,
	}

	logrus.WithFields(logrus.Fields{
		"appName": serverContext.Config.AppName,
	}).Info("starting server")
	go func() {
		logrus.Panic(serverContext.HttpServer.ListenAndServe())
	}()
	logrus.WithFields(logrus.Fields{
		"appName": serverContext.Config.AppName,
		"port": serverContext.Config.Port,
	}).Info("server is running")
}

// Returns a channel that sends a signal when one of the following system calls is detected:
//	* SIGHUP
//	* SIGINT
//	* SIGTERM
//	* SIGKILL
//	* SIGQUIT
func WaitForStopSignal() chan os.Signal {
	stop := make(chan os.Signal)
	signal.Notify(stop, os.Interrupt, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGKILL, syscall.SIGQUIT)
	return stop
}

// Shuts the http server supplied in the serverContext down
func Stop(serverContext *config.Context) {
	logrus.WithFields(logrus.Fields{
		"appName": serverContext.Config.AppName,
	}).Info("shutting down server")

	if err := serverContext.HttpServer.Shutdown(context.Background()); err != nil {
		logrus.WithFields(logrus.Fields{
			"appName": serverContext.Config.AppName,
		}).Error("failed to gracefully shutdown server")
	}

	logrus.WithFields(logrus.Fields{
		"appName": serverContext.Config.AppName,
	}).Info("gracefully shutdown server")
}