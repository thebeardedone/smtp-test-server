package utils

import (
	"encoding/json"
	"github.com/sirupsen/logrus"
	"net/http"
)

// Wrapper function which calls SendJSONResponse with http.StatusOk
func OnSuccess(writer http.ResponseWriter, payload interface{}) {
	SendJSONResponse(writer, http.StatusOK, payload)
}

// Wrapper function which calls SendJSONResponse with the provided error code and specifies an error message in the
// response
func OnError(writer http.ResponseWriter, code int, message string) {
	SendJSONResponse(writer, code, map[string]string{"error": message})
}

// Writes a json response with the specified error code and supplied payload
// An error is logged if the write fails
func SendJSONResponse(writer http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)

	writer.Header().Set("Content-Type", "application/json")
	writer.WriteHeader(code)
	_, writeError := writer.Write(response)
	if writeError != nil {
		logrus.WithField("error", writeError.Error()).Warn("failed to write response")
	}
}