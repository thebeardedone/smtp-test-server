package utils

import (
	"errors"
	"github.com/DusanKasan/parsemail"
	"github.com/emersion/go-smtp"
	"github.com/sirupsen/logrus"
	"gitlab.com/thebeardedone/smtp-test-server/config"
	"io"
	"strconv"
	"sync"
	"time"
)

const(
	appName = "test SMTP server"
)

// The backend that implements SMTP server methods
type SMTPBackEnd struct {
	Config			config.SMTP
	emails			[]parsemail.Email
	mutex			sync.Mutex
	SMTPServer		*smtp.Server
}

// Default constructor for SMTPBackEnd
func NewSMTPBackEnd(serverConfig config.SMTP) *SMTPBackEnd {
	smtpBackEnd := &SMTPBackEnd{
		Config: serverConfig,
	}
	server := smtp.NewServer(smtpBackEnd)

	server.Addr = ":" + strconv.Itoa(serverConfig.Port)
	server.Domain = serverConfig.Domain
	server.ReadTimeout = time.Duration(serverConfig.SMTPConnection.ReadTimeout) * time.Second
	server.WriteTimeout = time.Duration(serverConfig.SMTPConnection.WriteTimeout) * time.Second
	server.MaxMessageBytes = serverConfig.SMTPConnection.MaxMessageBytes
	server.MaxRecipients = serverConfig.SMTPConnection.MaxRecipients
	server.AllowInsecureAuth = true

	smtpBackEnd.SMTPServer = server

	return smtpBackEnd
}

// Starts the SMTPServer in a new go-routine
func (smtpBackEnd *SMTPBackEnd) Start() {
	logrus.WithFields(logrus.Fields{
		"appName": appName,
	}).Info("starting server")

	go func() {
		if err := smtpBackEnd.SMTPServer.ListenAndServe(); err != nil {
			logrus.Fatal(err)
		}
	}()
	logrus.WithFields(logrus.Fields{
		"appName": appName,
		"port": smtpBackEnd.Config.Port,
	}).Info("server is running")
}

// Calls close on a running SMTPServer instance
func (smtpBackEnd *SMTPBackEnd) Stop() {
	logrus.WithFields(logrus.Fields{
		"appName": appName,
	}).Info("shutting down server")

	if err := smtpBackEnd.SMTPServer.Close(); err != nil {
		logrus.WithFields(logrus.Fields{
			"appName": appName,
		}).Error("failed to gracefully shutdown server")
	}

	logrus.WithFields(logrus.Fields{
		"appName": appName,
	}).Info("gracefully shutdown server")
}

// Login handles a login command with username and password.
func (smtpBackEnd *SMTPBackEnd) Login(state *smtp.ConnectionState, username, password string) (smtp.Session, error) {
	if username != smtpBackEnd.Config.Credentials.Username || password != smtpBackEnd.Config.Credentials.Password {
		return nil, errors.New("invalid username or password")
	}
	return &Session{
		smtpBackEnd: smtpBackEnd,
	}, nil
}

// AnonymousLogin requires clients to authenticate using SMTP AUTH before sending emails
func (smtpBackEnd *SMTPBackEnd) AnonymousLogin(state *smtp.ConnectionState) (smtp.Session, error) {
	if smtpBackEnd.Config.Credentials.Username != "" && smtpBackEnd.Config.Credentials.Password != "" {
		return nil, smtp.ErrAuthRequired
	} else {
		return &Session{
			smtpBackEnd: smtpBackEnd,
		}, nil
	}
}

// A Session is returned after successful login.
type Session struct{
	smtpBackEnd	*SMTPBackEnd
}

func (s *Session) Mail(from string, opts smtp.MailOptions) error {
	return nil
}

func (s *Session) Rcpt(to string) error {
	return nil
}

// Data is parsed and is added to the email queue
func (s *Session) Data(reader io.Reader) error {
	parsedEmail, readError := parsemail.Parse(reader)
	if readError != nil {
		return readError
	}
	if len(parsedEmail.TextBody) > 0 && parsedEmail.TextBody[len(parsedEmail.TextBody) - 1] == '\r'{
		parsedEmail.TextBody = parsedEmail.TextBody[:len(parsedEmail.TextBody) - 1]
	}
	s.smtpBackEnd.AddEmail(parsedEmail)
	return nil
}

func (s *Session) Reset() {}

func (s *Session) Logout() error {
	return nil
}

// Adds an email to the queue
// Uses a mutex to ensure thread safety.
func (smtpBackEnd *SMTPBackEnd) AddEmail(email parsemail.Email) {
	smtpBackEnd.mutex.Lock()
	smtpBackEnd.emails = append(smtpBackEnd.emails, email)
	smtpBackEnd.mutex.Unlock()
}
// Retrieves the first email and removes it from the queue
// Uses a mutex to ensure thread safety.
func (smtpBackEnd *SMTPBackEnd) GetEmail() (parsemail.Email, error) {

	interval := 10 * time.Millisecond
	timePassed := time.Duration(0)

	for {
		time.Sleep(interval)
		if len(smtpBackEnd.emails) > 0 {
			smtpBackEnd.mutex.Lock()
			email := smtpBackEnd.emails[0]
			if len(smtpBackEnd.emails) == 1 {
				smtpBackEnd.emails = []parsemail.Email{}
			} else {
				smtpBackEnd.emails = smtpBackEnd.emails[1:len(smtpBackEnd.emails)]
			}
			smtpBackEnd.mutex.Unlock()
			return email, nil
		} else if timePassed > time.Duration(smtpBackEnd.Config.GetEmailTimeout) * time.Second {
			return parsemail.Email{}, errors.New("no email available")
		}
		timePassed += interval
	}
}

// Removes all emails from the queue
// Uses a mutex for thread safety.
func (smtpBackEnd *SMTPBackEnd) ClearEmails() {
	smtpBackEnd.mutex.Lock()
	smtpBackEnd.emails = []parsemail.Email{}
	smtpBackEnd.mutex.Unlock()
}